variable "region" {
  default = "ap-south-1"
}

variable "ami_id" {
  type = map(string)

  default = {
    ap-south-1    = "ami-006d3995d3a6b963b"
    eu-west-2    = "ami-132b3c7efe6sdfdsfd"
    eu-central-1 = "ami-9787h5h6nsn"
  }
}
